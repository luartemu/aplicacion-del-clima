// requireds
const { argv } = require('./config/yargs');
const { getLugarLatLn } = require('./lugar/lugar')
const { getClima } = require('./clima/clima')




const getInfo = async(direccion) => {
    try {
        const lugar = await getLugarLatLn(direccion);
        const clima = await getClima(lugar.lat, lugar.lng);
        return `EL clima de: ${direccion} es de: ${clima} C`
    } catch (e) {
        return `No hay informacion del clima para: ${direccion}`
    }
}


getInfo(argv.direccion)
    .then(console.log)
    .catch(console.error)