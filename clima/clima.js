const axios = require('axios');

const key = '0ae97952782d174da302ac89eacefd7d';

const getClima = async(lat, lon) => {


    const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${key}&units=metric`);


    return resp.data.main.temp;

};

module.exports = {
    getClima
}